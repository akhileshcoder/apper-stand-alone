import { Component, AfterViewInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
declare var AapperEditor: any;

const EndPoints = {
  APPER_BUILD_APP: "http://localhost:3200/secure/apper/build/application"
};
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  appJsonOption = {
    title: "App4pc",
    themeType: [
      {
        title: "SideBar",
        star: 5,
        desc:
          "Page navigation menu will be on left dwower and " +
          "after pulling it will show for navigation"
      },
      {
        title: "Bottom Menu",
        star: 4,
        desc:
          "Page navigation menu will be in bottom just like button" +
          " which will navigate to pages"
      },
      {
        title: "Top Menu",
        star: 3,
        desc:
          "Page navigation menu will be at top just like button" +
          " which will navigate to pages"
      },
      {
        title: "Tiles",
        star: 1,
        desc:
          "Page navigation menu will be as tiles which will navigate to pages"
      },
      {
        title: "List",
        star: 3,
        desc:
          "Page navigation menu will be as list which will navigate to pages"
      }
    ],
    fontFamily: [
      {
        title: "Default",
        val:
          "-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',sans-serif"
      },
      {
        title: "Arial",
        val: "Arial, Helvetica, sans-serif"
      },
      {
        title: "Arial Black",
        val: "'Arial Black', Gadget, sans-serif"
      },
      {
        title: "Bookman Old Style",
        val: "'Bookman Old Style', serif"
      },
      {
        title: "Comic Sans MS",
        val: "'Comic Sans MS', cursive"
      },
      {
        title: "Courier",
        val: "Courier, monospace"
      },
      {
        title: "Courier New",
        val: "'Courier New', Courier, monospace"
      },
      {
        title: "Garamond",
        val: "Garamond, serif"
      },
      {
        title: "Georgia",
        val: "Georgia, serif"
      },
      {
        title: "Impact",
        val: "Impact, Charcoal, sans-serif"
      },
      {
        title: "Lucida Console",
        val: "'Lucida Console', Monaco, monospace"
      },
      {
        title: "Lucida Sans Unicode",
        val: "'Lucida Sans Unicode', 'Lucida Grande', sans-serif"
      },
      {
        title: "MS Sans Serif",
        val: "'MS Sans Serif', Geneva, sans-serif"
      },
      {
        title: "MS Serif",
        val: "'MS Serif', 'New York', sans-serif"
      },
      {
        title: "Palatino Linotype",
        val: "'Palatino Linotype', 'Book Antiqua', Palatino, serif"
      },
      {
        title: "Symbol",
        val: "Symbol, sans-serif"
      },
      {
        title: "Tahoma",
        val: "Tahoma, Geneva, sans-serif"
      },
      {
        title: "Times New Roman",
        val: "'Times New Roman', Times, serif"
      },
      {
        title: "Trebuchet MS",
        val: "'Trebuchet MS', Helvetica, sans-serif"
      },
      {
        title: "Verdana",
        val: "Verdana, Geneva, sans-serif"
      },
      {
        title: "Webdings",
        val: "Webdings, sans-serif"
      },
      {
        title: "Wingdings",
        val: "Wingdings, 'Zapf Dingbats', sans-serif"
      }
    ],
    palettes: [
      {
        title: "Dark",
        val: "dark"
      },
      {
        title: "Light",
        val: "light"
      }
    ],
    appBuildOpt: ["Android", "IPhone", "Windows", "PC", "Browser Extension"]
  };
  appJson = {
    app: {
      icon: "Icon",
      name: "Apper",
      theme: "sidebar",
      description: "Hello world",
      style: {
        palette: "dark",
        background: "#ccc",
        fontFamilyBase: {
          title: "Default",
          val:
            "-apple-system,system-ui,BlinkMacSystemFont,'Segoe UI',Roboto,'Helvetica Neue',sans-serif"
        }
      },
      pages: []
    },
    noOfPages: 0,
    platform: "android"
  };
  loremEpsop =
    "" +
    "<h1>HTML Ipsum Presents</h1>" +
    // tslint:disable-next-line:max-line-length
    '<p><strong>Pellentesque habitant morbi tristique</strong> enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>' +
    "<h3>Header Level 3</h3>" +
    "<ul>" +
    "<li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>" +
    "<li>Aliquam tincidunt mauris eu risus.</li>" +
    "</ul>";
  showSteps = [false, false, false, false, false, false, false];
  stepsComplited = [false, false, false, false, false, false];
  pageEdit: any = false;
  tmpPg: any;
  currContent: any;
  editor: any;
  public mFormTitle;
  public fields = [];
  public mConfigField;

  public i18n = {
    47894: "47894",
    ABOUT: "About",
    ACCEPT: "Accept",
    ACCOUNT_NO: "Account No",
    ACCOUNTS: "Accounts",
    ACTIVE: "Active",
    ADD: "Add",
    ADD_EVENT: "Add Event",
    ADD_PAGE: "Add Page",
    ADD_PEOPLE: "Add People",
    ADD_TAG: "Add Tag",
    ADD_CONTACT: "Add contact",
    ADD_FIELD: "Add field",
    ADD_FORM: "Add form",
    ADD_NOTE: "Add note",
    ADD_PARTICIPANTS: "Add participants",
    ADD_TO_CONTACT: "Add to contact",
    ADDRESS: "Address",
    AGREE_TERM___CONDITION: "Agree term & condition",
    ALREADY_HAVE_AN_ACCOUNT: "Already have an account",
    ALTER_GROUP: "Alter group",
    ANALYTICS: "Analytics",
    ANSWER_SECURITY_QUESTIONS: "Answer security questions",
    APP_STORE: "App Store",
    APP_CALL_LOG: "App call log",
    APPER: "Apper",
    ARCHIVE: "Archive",
    ARCHIVED: "Archived",
    ATTACHMENTS: "Attachments",
    AUDIO: "Audio",
    AUDIO_CALL: "Audio call",
    AUDIO_CALL_FROM: "Audio Call from",
    AUDIO_CALL_ALERT: "Audio call alert",
    BACK: "Back",
    BACKGROUND: "Background",
    BACKGROUND_COLOR: "Background Color",
    BACKGROUND_IMAGE: "Background image",
    BILLS: "Bills",
    BIRTHDAY: "Birthday",
    BLOCK: "Block",
    BLOCK_PROFILE: "Block Profile",
    BOOK: "Book",
    BOOKS: "Books",
    BROWSER: "Browser",
    BORADCAST_NAME: "Broadcast name",
    BUILD_APP: "Build App",
    BUY_MESSAGE_PACK: "Buy message pack",
    BUY_STORAGE_PACK: "Buy storage pack",
    CALCULATOR: "Calculator",
    CALENDER: "Calender",
    CALL_LOG: "Call Log",
    CALL_FROM: "Call from",
    CALL_LOGS: "Call logs",
    CALLING_TO: "Calling to",
    CAMERA: "Camera",
    CANCEL: "Cancel",
    CAPTURE: "Capture",
    CAPTURE_IMAGE: "Capture Image",
    CHAT: "Chat",
    CHATBOX: "Chatbox",
    CHECKBOX: "Checkbox",
    CHOOSE_FONT_AND_COLOR: "Choose Font and Color",
    CHOOSE_FROM_GALLERY: "Choose from gallery",
    CHOOSE_FROM_DEVICE: "Choose from device",
    CHOOSE_TUNE: "Choose tune",
    CHOOSE: "Choose",
    CHOOSE_THEME: "Choose theme",
    CLEAR_CONVERSATION: "Clear conversation",
    CLEAR_LOG: "Clear log",
    CLOSE: "Close",
    CODE: "Code",
    COLOR: "Color",
    COLOR_PALLET: "Color Pallet",
    COMMON_GROUPS: "Common groups",
    COMPANY: "Company",
    CONFIG: "Config",
    CONTACT: "Contact",
    COPY: "Copy",
    CREATE_ADMIN: "Create admin",
    CREATE_APP: "Create app",
    DARK_THEME: "Dark Theme",
    DASHBOARD: "Dashboard",
    DATE: "Date",
    DATE_OF_BIRTH: "Date of birth",
    DATE_OF_BIRTH_IS_REQUIRED: "Date of birth is required",
    DATE_OF_BIRTH_INVALID: "Date of birth is not valid",
    DEFAULT: "Default",
    DELETE: "Delete",
    DON_T_HAVE_A_ACCOUNT__REGISTER: "Don't have a account, Register",
    DONE: "Done",
    DOWNLOAD_APP: "Download App",
    DRAFT: "Draft",
    EDIT: "Edit",
    EDIT_PAGES: "Edit Pages",
    EMAIL: "Email",
    EMAIL_SEPARATED_BY_COMA: "Emails separeted by comma",
    EMOJI: "Emoji",
    END_DATE: "End Date",
    END_CALL: "End call",
    ENGLISH: "English",
    ENTER_BORADCAST_NAME: "Enter broadcast name",
    ENTER_YOUR_MAIL: "Enter your email",
    ENTER_DEFAULT_VALUE: "Enter default value",
    ENTER_PLACEHOLDER: "Enter placeholder",
    ENTER_FIELD_NAME: "Enter field name",
    ENTER_GROUP_NAME: "Enter group name",
    ENTER_FIELD_TITLE: "Enter field title",
    ENTER_EMAIL: "Enter email",
    ENTER_PASSWORD: "Enter password",
    FEMALE: "Female",
    FIELD_PROPERTIES: "Field Properties",
    FONT_FAMILY: "Font Family",
    FORWARD: "Forward",
    FORM_NAME: "From Name",
    FROM_DEVICE: "From device",
    FROM_: "From:",
    GALLERY: "Gallery",
    GAME: "Game",
    GENDER: "Gender",
    GENDER_IS_REQUIRED: "Gender is required",
    GENERAL: "General",
    GOTO_CONVERSATION: "Goto conversation",
    GROUP_NAME: "Group name",
    HOME: "Home",
    ICON: "Icon",
    IM: "Im",
    IMAGE: "Image",
    INACTIVITY_TIME: "Inactivity time",
    INBOX: "Inbox",
    INFLOW: "Inflow",
    INFLOW_: "Inflow:",
    INIT_APP: "Init App",
    INSTALL: "Install",
    INSTALLED_APPS: "Installed Apps",
    INTERNET_CALL: "Internet call",
    INVALID_ADDRESS: "Invalid Address",
    INVALID_COMPANY_NAME: "Invalid Company name",
    INVALID_CODE: "Invalid code",
    INVALID_EMAIL: "Invalid email",
    INVALID_JOB_TITLE: "Invalid job title",
    INVALID_MOBILE_NUMBER: "Invalid mobile number",
    INVALID_MOBILE_OR_EMAIL: "Invalid mobile or email",
    INVALID_PASSWORD: "Invalid password",
    INVALID_PASSWORD_MIN_6: "Min length should be 6",
    INVALID_RELATIONSHIP: "Invalid relationship",
    INVALID_URL: "Invalid url",
    INVALID_VALUE: "Invalid value",
    INVITE: "Invite",
    JOB_TITLE: "Job title",
    LABEL: "Label",
    LANGUAGE___INPUT: "Language & Input",
    LIGHT_THEME: "Light Theme",
    LIVE_LOCATION: "Live Location",
    LIKE: "Like",
    LOAD_MORE: "Load more",
    LOADING___: "Loading...",
    LOCATION: "Location",
    LOGIN: "login",
    LOGO: "Logo",
    LOGOUT: "Logout",
    LOW_SMS_BALANCE: "Low message balance",
    LOW_SMS_BALANCE_DESC:
      "Add credits to send message to non registered user (per day 10 message is free)",
    MALE: "Male",
    MAIL: "Mail",
    MEDIUM: "Medium",
    MEDIA: "Media",
    MESSAGE_ALERT: "Message alert",
    MOBILE: "Mobile",
    MOBILE___EMAIL: "Mobile / Email",
    MOBILE_CALL_LOG: "Mobile call log",
    MOBILE_NUMBER_IS_REQUIRED: "Mobile number is required",
    MODEL_TITLE: "Model title",
    MOVIES: "Movies",
    MUSIC: "Music",
    MY_APPS: "My apps",
    MY_MONEY: "My money",
    MY_PROFILE: "My profile",
    MY_CONTACTS: "My contacts",
    NAME: "Name",
    NAME_IS_REQUIRED: "Name is required",
    NEW_CALL: "New call",
    NEW_GROUP: "New Group",
    NEW_MAIL: "New Mail",
    NEW_BROADCAST: "New broadcast",
    NEW_CONTACT: "New contact",
    NEXT: "Next",
    NO_CONTACTS: "No contacts",
    NO_CONTACT_SELECTED: "No contacts selected",
    NO_RESULT_FOUND: "No Result found",
    NO_INSTALLED_APP_AVAILABLE: "No installed app available",
    NONE: "None",
    NOTEPAD: "Notepad",
    NOTES: "Notes",
    NOTE_SAVED: "Note saved",
    NOTIFICATION: "Notification",
    NUMBER: "Number",
    OR_REGISTER_WITH: "Or resister with",
    ONLINE_VISIBILITY: "Online Visibility",
    OPEN: "Open",
    OTHER: "Other",
    OUTFLOW: "Outflow",
    OUTFLOW_: "Outflow:",
    OUTSTANDING_AMMOUNT_: "Outstanding Ammount:",
    PARTICIPANTS: "Participants",
    PASSWORD: "Password",
    PASSWORD_LOCK: "Password Lock",
    PASSWORD_IS_REQUIRED: "Password is required",
    PASSWORD_RESET_LINK__WILL_NEED_BOTH_MAIL_AND_MOBILE_VERIFICATION:
      "Password reset link  will need both mail and mobile verification",
    PATTERN_LOCK: "Pattern Lock",
    PEOPLE: "People",
    PEOPLES: "Peoples",
    PHONETIC_FIRST: "Phonetic first",
    PHONETIC_LAST: "Phonetic last",
    PLACEHOLDER: "Placeholder",
    POST: "Post",
    POST_MUST_HAVE_TEXT_OR_IMAGE: "Post must have text or image",
    PREVIEW: "Preview",
    PREVIEW_IMAGE: "Preview Image",
    PRIVACY: "Privacy",
    PROCESSING: "Processing",
    PROCESSING___: "Processing...",
    PROFILE: "Profile",
    PROFILE_VISIBILITY: "Profile Visibility",
    PUBLIC: "Public",
    RECEIVE: "Receive",
    RECORD: "Record",
    RECORDER: "Recorder",
    REGISTER: "Register",
    REJECT: "Reject",
    RELATIONSHIP: "Relationship",
    REMAINING: "Remaining",
    REMEMBER: "Remember",
    REMOVE: "Remove",
    REMOVE_ADMIN: "Remove admin",
    RENAME: "Rename",
    RERECORD: "Re record",
    RE_RECORD: "Re record",
    RETAKE: "Retake",
    SAVE: "Save",
    SAVING: "Saving",
    SAVE_TO_GALLERY: "Save to Gallery",
    SEARCH: "Search",
    SEARCHING___: "Searching...",
    SECURITY: "Security",
    SELECTED: "Selected",
    SELECT: "Select",
    SEND: "Send",
    SENT: "Sent",
    SET_STATUS: "Set status",
    SETTINGS: "Settings",
    SHARE: "Share",
    SHARE_FROM_GALLERY: "Share from gallery",
    SILENT: "Silent",
    SMS: "SMS",
    STAR: "Star",
    START: "Start",
    START_DATE: "Start Date",
    STATUS: "Status",
    STOP: "Stop",
    STORAGE: "Storage",
    STORE: "Store",
    SUBJECT: "Subject",
    SWING: "Swing",
    Fly: "Fly",
    TEXT: "Text",
    THEME: "Theme",
    TITLE: "Title",
    TO_: "To:",
    TODAY: "Today",
    TOTAL_STARED: "Total stared",
    TOTAL: "Total",
    TOTAL_: "Total:",
    TRANSACTIONS: "Transactions",
    TRASH: "Trash",
    TYPE: "Type",
    TYPING__: "Typing...",
    UNARCHIVED: "Unarchived",
    UNBLOCK: "Unblock",
    UNINSTALL: "Uninstall",
    UNSTARED: "unstarred",
    UTILS: "Utils",
    VALIDITY: "Validity",
    VENDOR: "Vendor",
    VIDEO: "Video",
    VIDEO_CALL: "Video call",
    VIDEO_CALL_FROM: "Video Call from",
    VIDEO_CALL_ALERT: "Video call alert",
    VIEWER: "Viewer",
    WEB: "Web",
    WEBSITE: "Website",
    WITHOUT_LOGIN_YOU_WON_T_ABLE_TO_ACCESS_SOME_APPS:
      "Without login you won't able to access some apps",
    YOU_MUST_ACCEPT_TERM___CONDITION: "You must accept term & condition",
    YOU_MUST_ENTER_THE_ACTIVATION_CODE: "You must enter the activation code",
    FRAN_AIS: "français",
    HINDI: "हिंदी",
    HAVE_A_COFFEE_TILL_WE_ARE_WORKING_ON_APP__WE_WILL_MAIL_MESSAGE_YOU_WHEN_APP_WILL_BE_READY__NOTE__IT_MIGHT_TAKE_AROUND_5_MINUTS_:
      "Have a coffee till we are working on app. We will mail/message you when app will be ready. Note: it might take around 5 minuts"
  };
  constructor(public http: HttpClient) {
    this.addNPages();
    this.addField();
  }

  ngAfterViewInit() {}

  chhooseTheme(itm) {
    this.appJson.app.theme = itm.toLowerCase();
    this.togStep(2);
  }

  togStep(i) {
    const tmp = this.showSteps[i];
    this.showSteps = [false, false, false, false, false, false];
    this.showSteps[i] = !tmp;
    this.setStepsComplited(i);
  }
  setStepsComplited(i) {
    if (
      this.appJson.app &&
      this.appJson.app.name &&
      this.appJson.app.icon &&
      this.appJson.app.style.background
    ) {
      this.stepsComplited[0] = true;
    } else { this.stepsComplited[0] = false; }

    if (this.appJson.app.theme) { this.stepsComplited[1] = true; } else { this.stepsComplited[1] = false; }

    if (this.appJson.app.style.fontFamilyBase && this.appJson.app.style.palette) {
      this.stepsComplited[2] = true;
    } else { this.stepsComplited[2] = false; }

    if (this.appJson.app.style.fontFamilyBase && this.appJson.app.style.palette) {
      this.stepsComplited[3] = true;
    } else { this.stepsComplited[3] = false; }

    this.stepsComplited[4] = true;

    if (i === 5) { this.stepsComplited[5] = true; }
  }

  removePage(p) {
    this.appJson.app.pages.splice(this.appJson.app.pages.indexOf(p), 1);
    this.appJson.noOfPages--;
  }

  buildApp(i) {
    this.appJson.platform = i;
    this.togStep(6);
    const app: any = JSON.parse(JSON.stringify(this.appJson));

    if (this.fields.length > 0) {
      app.app.pages.push({
        title: this.mFormTitle,
        // tslint:disable-next-line:variable-name
        fields: this.fields.map(_f => ({
          key: _f.name.replace(/[^A-Za-z0-9_]/g, "_"),
          type: "input",
          templateOptions: {
            type: _f.type,
            label: _f.name,
            placeholder: _f.placeholder
          }
        }))
      });
    }
    this.http
      .post(EndPoints.APPER_BUILD_APP, app)
      .toPromise()
      .then(data => {
        console.log('app');
        window.alert(JSON.stringify(app));
      })
      .catch(err => {});
  }

  opnMdl(ind) {
    this.pageEdit = true;
    this.currContent = this.appJson.app.pages[ind].content;
    this.tmpPg = [ind, this.appJson.app.pages[ind]];
    if ((window as any).CKEDITOR.instances.inputContent) {
      (window as any).CKEDITOR.instances.inputContent.destroy();
      (window as any).CKEDITOR.replace("inputContent");
    }
  }

  savePageEdit() {
    this.pageEdit = false;
    this.appJson.app.pages[this.tmpPg[0]].content = ((
      window
    ) as any).CKEDITOR.instances.inputContent.getData();
  }

  addField() {
    // tslint:disable-next-line:variable-name
    const _mField_ = {
      type: "text",
      name: `field_${this.fields.length}`,
      placeholder: ""
    };
    this.fields.push(_mField_);
  }

  addNPages() {
    this.appJson.noOfPages++;

    if (!this.appJson.app.pages) { this.appJson.app.pages = []; }

    this.appJson.app.pages.push({
      title: "Page Title",
      content: this.loremEpsop
    });
  }
}
